Scones
======

Ingredients
-----------

* 8 oz plain flour
* pinch of salt
* 1 oz butter
* 1 level tsp bicarb
* 2 level tsp cream of tartar

Method
------

1. Mix all together for 90 seconds with approx 5 oz water (cold).

2. Lie out on a floured tray.

3. Brush the tops with milk.

4. 230°C oven for 10 mins.

